; scale quantiser
; thom ash 2008

.INCLUDE "m48def.inc"

;declare lookup table in SRAM
.DSEG
.ORG 0x100
table:
.BYTE 0x100
table_end:

;register names
.CSEG
.DEF temp = R16
.DEF select = R17
.DEF roundto = R18
.DEF lastdac = R19
.DEF lastexttrig = R20
.DEF trigtimer = R21

;trigger length must be a multiple of 4
.SET triglength = 80



;interrupt vectors
.ORG 0x0000
rjmp reset
.ORG 0x0015
rjmp sample

;reset 
.ORG 0x001a
reset:
;set up stack
ldi temp, high(RAMEND)
out SPH,temp
ldi temp, low(RAMEND)
out SPL,temp
;enable interrupts
sei

;enable sleep 
ldi temp, 0b00000011
out SMCR, temp

;init I/O
;DAC on port D
ldi temp, 0b11111111
out DDRD, temp
ldi temp, 0b00000000
out PORTD, temp

;ADC on C0, generated clock on C5
ldi temp, 0b00100000
out DDRC, temp
ldi temp, 0b00000000
out PORTC, temp

;external clock on B0, selector data on B1-2, selector address on B3-5
ldi temp, 0b00111000
out DDRB, temp
ldi temp, 0b00000111
out PORTB, temp

;init ADC
LDI temp, 0b00100000
STS ADMUX, temp

LDI temp, 0b10001111
STS ADCSRA, temp

LDI temp, 0b00000000
STS ADCSRB, temp

LDI temp, 0b00000001
STS DIDR0, temp

;init registers for lookup table generation
LDI XL, low(table)
LDI XH, high(table)
ldi select, 0b00000000
ldi roundto, 0b00000000


;MAIN - active trigger, round down
main_active_down:

	;get a switch state to process
	;send address to selector
	mov temp, select
	lsl temp
	lsl temp
	lsl temp
	andi temp, 0b00111111
	ori temp, 0b00000111
	out PORTB, temp
	
	;give it some time to settle
	;process a sample from the ADC

	;start conversion 
	LDI temp, 0b11001111
	STS ADCSRA, temp
	sleep		;wake up on interrupt

	;DEBUG!!!!
	;ldi temp, 0b00000001
	;out portd, temp

	;countdown trig timer
	ldi temp, 0b00000000
	cpi trigtimer, 0x00 ;if it's not zero,
	breq skip1_active_down		
		subi trigtimer, 4	;decrement
		;dec trigtimer	;decrement


		brne skip1_active_down			;if it is zero now,
			out portc, temp	 ;trigger ends (C5 goes low)
	skip1_active_down:


	;read the data from selector
	in temp, PINB
	sbrc select, 3	;if we're looking at the second plexer,
		lsr temp	;shift input. now data we want is in bit 1

	cpi select, 12	;see what kind of switch we're looking at
	breq round_active_down		;rounding mode switch
	brlo note_active_down		;one of the note switches
		

	;TRIGGERING MODE - ACTIVE/PASSIVE
	clr select		;back to first switch
	sbrc temp, 1	;if triggering has been set to passive
		rjmp main_passive_down	;go to other loop

	rjmp main_active_down		;otherwise continue with this one

	;ROUNDING MODE - UP/DOWN
	round_active_down:
	inc select		;move on to next switch
	sbrs temp, 1	;if rounding has been set to up
		rjmp main_active_up		;go to other loop

	rjmp main_active_down		;otherwise continue with this one
	
	;NOTE - C to B
	note_active_down:
	inc select				;move on to next switch
	sbrc temp ,1			;if this note is active,
		mov roundto, XL		;update 'closest match'

	st X, roundto		;write 'closest match' to table
	inc XL				;next table address
	brne main_active_down	;if it didn't roll over, we're done

	clr select		;if it did, make sure we're pointing at C

rjmp main_active_down ;and we're done



;MAIN - active trigger, round up
main_active_up:

	;get a switch state to process
	;send address to selector
	mov temp, select
	lsl temp
	lsl temp
	lsl temp
	andi temp, 0b00111111
	ori temp, 0b00000111
	out PORTB, temp
	
	;give it some time to settle
	;process a sample from the ADC

	;start conversion
	LDI temp, 0b11001111
	STS ADCSRA, temp
	sleep		;wake up on interrupt

	;DEBUG!!!!
	;ldi temp, 0b00000010
	;out portd, temp

	;countdown trig timer
	ldi temp, 0b00000000
	cpi trigtimer, 0x00 ;if it's not zero,
	breq skip1_active_up		
		subi trigtimer, 4	;decrement
		;dec trigtimer	;decrement

		brne skip1_active_up			;if it is zero now,
			out portc, temp	 ;trigger ends (C5 goes low)
	skip1_active_up:

	;read the data from selector
	in temp, PINB
	sbrc select, 3	;if we're looking at the second plexer,
		lsr temp	;shift input. now data we want is in bit 1

	cpi select, 12	;see what kind of switch we're looking at
	breq round_active_up		;rounding mode switch
	brlo note_active_up			;one of the note switches
		

	;TRIGGERING MODE - ACTIVE/PASSIVE
	dec select		;move on to previous switch
	sbrc temp, 1	;if triggering has been set to passive
		rjmp main_passive_up	;go to other loop

	rjmp main_active_up		;otherwise continue with this one

	;ROUNDING MODE - UP/DOWN
	round_active_up:
	dec select		;move on to previous switch
	sbrc temp, 1	;if rounding has been set to down
		rjmp main_active_down		;go to other loop

	rjmp main_active_up		;otherwise continue with this one
	
	;NOTE - C to B
	note_active_up:
	dec select		;move on to previous switch
	cpi select, 0xff	;if it rolls over 
	brne PC + 2;
		ldi select, 13	;back to mode switch
	sbrc temp ,1			;if this note is active,
		mov roundto, XL		;update 'closest match'

	st X, roundto		;write 'closest match' to table
	dec XL				;next table address
	cpi XL, 0xff
	brne main_active_up	;if it didn't roll over, we're done

	ldi select, 3		;if it did, make sure we're pointing at D#

rjmp main_active_up ;and we're done



;MAIN - passive trigger, round down
main_passive_down:

	;get a switch state to process
	;send address to selector
	mov temp, select
	lsl temp
	lsl temp
	lsl temp
	andi temp, 0b00111111
	ori temp, 0b00000111
	out PORTB, temp
	
	;give it some time to settle
	
	in temp, PINB	;check ext trig status
	com temp
	push temp		;save clean version for later
	com temp		
	and temp, lastexttrig	;temp bit 0 is set if b0 just went low
	pop lastexttrig		;save clean version for next time

	sbrs temp, 0	;if temp bit 0 is clear,
		rjmp skip2_down		;skip ADC sampling

	;process a sample from the ADC
	;start conversion
	LDI temp, 0b11001111
	STS ADCSRA, temp
	sleep		;wake up on interrupt

	skip2_down:
	;DEBUG!!!!
	;ldi temp, 0b00000100
	;out portd, temp

	;countdown trig timer
	ldi temp, 0b00000000
	cpi trigtimer, 0x00 ;if it's not zero,
	breq skip1_passive_down		
		dec trigtimer	;decrement

		brne skip1_passive_down			;if it is zero now,
			out portc, temp	 ;trigger ends (C5 goes low)
	skip1_passive_down:

	;read the data from selector
	in temp, PINB
	sbrc select, 3	;if we're looking at the second plexer,
		lsr temp	;shift input. now data we want is in bit 1

	cpi select, 12	;see what kind of switch we're looking at
	breq round_passive_down		;rounding mode switch
	brlo note_passive_down		;one of the note switches
		

	;TRIGGERING MODE - ACTIVE/PASSIVE
	clr select		;back to first switch
	sbrs temp, 1	;if triggering has been set to active
		rjmp main_active_down	;go to other loop

	rjmp main_passive_down		;otherwise continue with this one

	;ROUNDING MODE - UP/DOWN
	round_passive_down:
	inc select		;move on to next switch
	sbrs temp, 1	;if rounding has been set to up
		rjmp main_passive_up		;go to other loop

	rjmp main_passive_down		;otherwise continue with this one
	
	;NOTE - C to B
	note_passive_down:
	inc select				;move on to next switch
	sbrc temp ,1			;if this note is active,
		mov roundto, XL		;update 'closest match'

	st X, roundto		;write 'closest match' to table
	inc XL				;next table address
	brne main_passive_down	;if it didn't roll over, we're done

	clr select		;if it did, make sure we're pointing at C

rjmp main_passive_down ;and we're done



;MAIN - passive trigger, round up
main_passive_up:

	;get a switch state to process
	;send address to selector
	mov temp, select
	lsl temp
	lsl temp
	lsl temp
	andi temp, 0b00111111
	ori temp, 0b00000111
	out PORTB, temp
	
	;give it some time to settle


	in temp, PINB	;check ext trig status
	com temp
	push temp		;save clean version for later
	com temp		
	and temp, lastexttrig	;temp bit 0 is set if b0 just went low
	pop lastexttrig		;save clean version for next time

	sbrs temp, 0	;if temp bit 0 is clear,
		rjmp skip2_up		;skip ADC sampling

	;process a sample from the ADC

	;start conversion
	LDI temp, 0b11001111
	STS ADCSRA, temp
	sleep		;wake up on interrupt
	skip2_up:
	;DEBUG!!!!
;	ldi temp, 0b00001000
;	out portd, temp


	;countdown trig timer
	ldi temp, 0b00000000
	cpi trigtimer, 0x00 ;if it's not zero,
	breq skip1_passive_up		
		dec trigtimer	;decrement

		brne skip1_passive_up			;if it is zero now,
			out portc, temp	 ;trigger ends (C5 goes low)
	skip1_passive_up:
	;read the data from selector
	in temp, PINB
	sbrc select, 3	;if we're looking at the second plexer,
		lsr temp	;shift input. now data we want is in bit 1

	cpi select, 12	;see what kind of switch we're looking at
	breq round_passive_up		;rounding mode switch
	brlo note_passive_up		;one of the note switches
		

	;TRIGGERING MODE - ACTIVE/PASSIVE
	dec select		;move on to previous switch
	sbrs temp, 1	;if triggering has been set to active
		rjmp main_active_up	;go to other loop

	rjmp main_passive_up		;otherwise continue with this one

	;ROUNDING MODE - UP/DOWN
	round_passive_up:
	dec select		;move on to previous switch
	sbrc temp, 1	;if rounding has been set to down
		rjmp main_passive_down		;go to other loop

	rjmp main_passive_up		;otherwise continue with this one
	
	;NOTE - C to B
	note_passive_up:
	dec select		;move on to previous switch
	cpi select, 0xff	;if it rolls over 
	brne PC + 2;
		ldi select, 13	;back to mode switch
	sbrc temp ,1			;if this note is active,
		mov roundto, XL		;update 'closest match'

	st X, roundto		;write 'closest match' to table
	dec XL				;next table address
	cpi XL, 0xff
	brne main_passive_up	;if it didn't roll over, we're done

	ldi select, 3		;if it did, make sure we're pointing at D#


rjmp main_passive_up ;and we're done

;interrupt handler
sample:

PUSH XL
push temp


IN lastdac, PORTD; save old value

LDS XL, ADCH	;sampled data points to lookup table
LD temp, X		;retrieve value
OUT PORTD, temp	;to DAC

cp temp, lastdac	;has it changed?
breq sameold			;if it has,
	ldi temp, 0b00100000		;generate a trigger (raise C5)
	out portc, temp
	ldi trigtimer, triglength	;and start counting down
sameold:

pop temp
POP XL

RETI
