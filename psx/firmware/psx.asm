;psx controller interface
;thom ash 2007

.INCLUDE "m48def.inc"

;declare packet buffers in SRAM
.SET b_size = 12
.DSEG
.ORG 0x100
data_b:
.BYTE b_size
cmd_b:
.BYTE b_size
clock_b:
.BYTE b_size
att_b:
.BYTE b_size
prev_trig:
.BYTE 1

;useful parts of SRAM
.SET type = data_b + 2
.SET ready = data_b + 3
.SET others = data_b + 4
.SET gatetrig = data_b + 5
.SET xright = data_b + 6
.SET yright = data_b + 7
.SET xleft = data_b + 8
.SET yleft = data_b + 9

;data rate
.SET speed = 0x60

;register names
.CSEG
.DEF temp = R16
.def temp2 = R17
.DEF ibit = R18
.DEF count = R19
.DEF data_r = R20
.DEF cmd_r = R0
.DEF clock_r = R1
.DEF att_r = R2

;initialize I/O lines
;psxpad on D0 - D3
;digital outs on port C
;analog outs on B1, B2, D5 and D6
LDI temp, 0b00000110
OUT DDRB, temp
LDI temp, 0b11111111
OUT DDRC, temp
LDI temp, 0b11111110
OUT DDRD, temp
LDI temp, 0b00000001
OUT PORTD, temp
LDI temp, 0b00000000
OUT PORTC, temp
OUT PORTB, temp

;setup timers 0 and 1 for 8bit fast PWM
LDI temp, 0b10110011
OUT TCCR0A, temp
LDI temp, 0b00000001
OUT TCCR0B, temp
LDI temp, 0b10110001
STS TCCR1A, temp
LDI temp, 0b00001001
STS TCCR1B, temp
LDI temp, 0b00000000
STS TCCR1C, temp

;initialize packet buffers in sram
;DATA (empty for now)
LDI YL, low(data_b)
LDI YH, high(data_b)

ST Y+, temp
ST Y+, temp
ST Y+, temp

ST Y+, temp
ST Y+, temp
ST Y+, temp

ST Y+, temp
ST Y+, temp
ST Y+, temp

ST Y+, temp
ST Y+, temp
ST Y+, temp

LDI YL, low(cmd_b)
LDI YH, high(cmd_b)

;CMD
ST Y+, temp
LDI temp, 0x01
ST Y+, temp
LDI temp, 0x42
ST Y+, temp

LDI temp, 0x00
ST Y+, temp
ST Y+, temp
ST Y+, temp

ST Y+, temp
ST Y+, temp
ST Y+, temp

ST Y+, temp
ST Y+, temp
ST Y+, temp

LDI YL, low(clock_b)
LDI YH, high(clock_b)

;CLOCK

ST Y+, temp
LDI temp, 0b11111111	
ST Y+, temp
ST Y+, temp

ST Y+, temp
ST Y+, temp
ST Y+, temp

ST Y+, temp
ST Y+, temp
ST Y+, temp

ST Y+, temp
LDI temp, 0b00000000
ST Y+, temp
ST Y+, temp

LDI YL, low(att_b)
LDI YH, high(att_b)

LDI temp, 0b00001111
ST Y+, temp
LDI temp, 0b11111111
ST Y+, temp
ST Y+, temp

ST Y+, temp
ST Y+, temp
ST Y+, temp

ST Y+, temp
ST Y+, temp
ST Y+, temp

ST Y+, temp
LDI temp, 0b11110000
ST Y+, temp
LDI temp, 0b00000000
ST Y+, temp

;get registers ready for main loop
LDI YL, low(data_b)
LDI YH, high(data_b)
LDI XL, low(data_b)
LDI XH, high(data_b)

;start main loop
main:

	;start byte loop
	byte:
		
		;load byte of output buffers into output registers
		LDD att_r, Y+b_size*3
		LDD clock_r, Y+b_size*2
		LDD cmd_r, Y+b_size

		;clear input register for input byte
		CLR data_r

		CLR ibit
		;start bit loop
		bit:

			;put LSB of output registers on output pins
			
			LDI temp, 0b00000001
			
			SBRS att_r, 0
				ORI temp, 0b00001000

			SBRS clock_r, 0
				ORI temp, 0b00000100

			SBRC cmd_r, 0
				ORI temp, 0b00000010

			OUT PORTD, temp
			
			;kill some time
			LDI count, speed
			waste:
					INC count
					BRNE waste



			;shift input register right(clears msb)
			LSR data_r

			;read DATA to msb of input register
			IN temp, PIND

			SBRC temp, 0
				ORI data_r, 0b10000000

			;send CLOCK high
			SBI PORTD, 2

			;shift output registers right
			LSR att_r
			LSR clock_r
			LSR cmd_r

			;kill some more time?
			LDI count, speed
			waste2:
					INC count
					BRNE waste2


		;end bit loop
		INC ibit
		CPI ibit, 8
		BRNE bit
		
		;write input register to input buffer
		ST Y+, data_r
		
		
	CPI YL, b_size ;check for end of buffer 
	BRNE byte ;another byte
	
	CLR YL;back to start of buffer

	;digital outs
	LDS temp, gatetrig
	LDS temp2, prev_trig
	COM temp2
	OR temp2, temp  ;bits 0-3 of temp2 are 0 if that trigger has just been hit
	
	COM temp2
	MOV temp, temp2
	LSL temp
	OR temp2, temp  ;bits 1 and 3 of temp2 are now 1 for each trigger channel

	LDS temp, gatetrig
	SWAP temp
	ORI temp, 0b11110000	;gates are now in place

	SBRC temp2, 3
		ANDI temp, 0b11101111
	SBRC temp2, 1
		ANDI temp, 0b11011111


	OUT PORTC, temp

	;save trigger status
	LDS temp2, gatetrig
	STS prev_trig, temp2
	
	;PWM analogue outs
	LDS temp, yleft
	OUT OCR0A, temp

	LDS temp, xleft
	OUT OCR0B, temp

	LDS temp, yright
	STS OCR1AL, temp

	LDS temp, xright
	STS OCR1BL, temp	

;end main loop
RJMP main
