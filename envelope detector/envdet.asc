Version 4
SHEET 1 1752 832
WIRE 16 -1216 16 -1248
WIRE 32 -1216 16 -1216
WIRE 464 -1184 384 -1184
WIRE 592 -1184 544 -1184
WIRE 32 -1168 32 -1216
WIRE 240 -1168 128 -1168
WIRE 256 -1168 240 -1168
WIRE 352 -1168 336 -1168
WIRE 912 -1152 624 -1152
WIRE 1184 -1152 1072 -1152
WIRE 240 -1120 240 -1168
WIRE 288 -1120 240 -1120
WIRE 352 -1104 352 -1168
WIRE 384 -1104 384 -1184
WIRE 384 -1104 352 -1104
WIRE 416 -1104 384 -1104
WIRE 176 -1088 144 -1088
WIRE 544 -1088 480 -1088
WIRE 624 -1088 624 -1152
WIRE 640 -1088 624 -1088
WIRE 880 -1088 864 -1088
WIRE 912 -1088 912 -1152
WIRE 912 -1088 880 -1088
WIRE 928 -1088 912 -1088
WIRE 1056 -1088 992 -1088
WIRE 1072 -1088 1072 -1152
WIRE 1088 -1088 1072 -1088
WIRE 288 -1072 288 -1120
WIRE 288 -1072 240 -1072
WIRE 320 -1072 288 -1072
WIRE 416 -1072 368 -1072
WIRE 592 -1072 592 -1184
WIRE 624 -1072 592 -1072
WIRE 720 -1072 704 -1072
WIRE 800 -1072 800 -1088
WIRE 1184 -1072 1184 -1152
WIRE 1184 -1072 1152 -1072
WIRE 1264 -1072 1264 -1168
WIRE 1264 -1072 1184 -1072
WIRE 32 -1056 32 -1104
WIRE 176 -1056 32 -1056
WIRE 544 -1056 544 -1088
WIRE 624 -1056 624 -1072
WIRE 624 -1056 608 -1056
WIRE 640 -1056 624 -1056
WIRE 928 -1056 912 -1056
WIRE 1056 -1056 1056 -1088
WIRE 1072 -1056 1056 -1056
WIRE 1088 -1056 1072 -1056
WIRE 368 -1040 368 -1072
WIRE 320 -1024 320 -1072
WIRE 736 -992 608 -992
WIRE 880 -992 880 -1088
WIRE 880 -992 736 -992
WIRE 32 -944 32 -1056
WIRE 144 -928 144 -1088
WIRE 320 -928 320 -944
WIRE 320 -928 144 -928
WIRE 320 -912 320 -928
WIRE 608 -912 608 -992
WIRE 608 -912 576 -912
WIRE 736 -912 736 -992
WIRE 736 -912 704 -912
WIRE 880 -912 880 -992
WIRE 880 -912 832 -912
WIRE 992 -912 880 -912
WIRE 1072 -912 1072 -1056
WIRE 1120 -912 1072 -912
WIRE 512 -880 480 -880
WIRE 640 -880 608 -880
WIRE 768 -880 736 -880
WIRE 320 -816 320 -832
WIRE 736 -784 736 -880
WIRE 736 -784 656 -784
WIRE 912 -784 912 -1056
WIRE 1280 -784 912 -784
WIRE 784 -720 768 -720
WIRE 128 -704 128 -1168
WIRE 128 -704 64 -704
WIRE 480 -704 480 -880
WIRE 608 -704 608 -880
WIRE 608 -704 480 -704
WIRE 656 -704 656 -784
WIRE 656 -704 608 -704
WIRE 944 -688 768 -688
WIRE 944 -640 944 -688
WIRE 960 -640 944 -640
WIRE 1040 -640 1024 -640
WIRE 736 -608 544 -608
WIRE 944 -592 944 -640
WIRE 944 -592 912 -592
WIRE 176 -576 112 -576
WIRE 1232 -576 1168 -576
WIRE 112 -560 112 -576
WIRE 288 -560 240 -560
WIRE 384 -560 352 -560
WIRE 448 -560 432 -560
WIRE 1040 -560 1040 -640
WIRE 1056 -560 1040 -560
WIRE 176 -544 144 -544
WIRE 544 -544 544 -608
WIRE 1184 -544 1168 -544
WIRE 1232 -544 1232 -576
WIRE 384 -528 384 -560
WIRE 448 -528 384 -528
WIRE 64 -512 64 -704
WIRE 144 -512 144 -544
WIRE 144 -512 64 -512
WIRE 384 -512 384 -528
WIRE 640 -496 640 -544
WIRE 688 -496 640 -496
WIRE 736 -496 736 -608
WIRE 1184 -480 1184 -544
WIRE 1184 -480 848 -480
WIRE 1280 -480 1280 -784
WIRE 1280 -480 1184 -480
WIRE 640 -464 640 -496
WIRE 688 -464 688 -496
WIRE 736 -464 688 -464
WIRE -352 -224 -400 -224
WIRE -224 -224 -272 -224
WIRE -400 -96 -400 -224
WIRE -400 -96 -432 -96
WIRE -352 -96 -400 -96
WIRE -240 -80 -288 -80
WIRE -224 -80 -224 -224
WIRE -224 -80 -240 -80
WIRE 432 -80 432 -96
WIRE 496 -80 432 -80
WIRE 608 -80 576 -80
WIRE 656 -80 608 -80
WIRE 800 -80 736 -80
WIRE -352 -64 -384 -64
WIRE -384 -32 -384 -64
WIRE 800 -32 800 -80
WIRE 1328 0 1184 0
WIRE 912 16 880 16
WIRE 1008 16 992 16
WIRE 1120 16 1072 16
WIRE -432 32 -432 -96
WIRE 1184 64 1184 0
WIRE 1216 64 1184 64
WIRE 464 80 352 80
WIRE 1328 80 1328 0
WIRE 1328 80 1280 80
WIRE 1376 80 1328 80
WIRE 1440 80 1376 80
WIRE 592 96 528 96
WIRE 912 96 880 96
WIRE 1008 96 992 96
WIRE 1120 96 1120 16
WIRE 1120 96 1072 96
WIRE 1216 96 1120 96
WIRE 464 112 352 112
WIRE 1440 112 1440 80
WIRE 1120 128 1120 96
WIRE 1200 128 1120 128
WIRE 32 144 -80 144
WIRE 128 144 112 144
WIRE 1120 160 1120 128
WIRE 1200 176 1200 128
WIRE -432 208 -432 112
WIRE -432 208 -464 208
WIRE -240 208 -240 -80
WIRE -80 208 -80 144
WIRE -48 208 -80 208
WIRE 48 208 16 208
WIRE 736 208 704 208
WIRE 864 208 816 208
WIRE -464 240 -464 208
WIRE 576 288 544 288
WIRE 704 288 704 208
WIRE 704 288 656 288
WIRE 1120 288 1120 224
WIRE 1200 288 1200 256
WIRE 1200 288 1120 288
WIRE -240 304 -240 208
WIRE -192 304 -240 304
WIRE -80 304 -80 208
WIRE -80 304 -112 304
WIRE -48 304 -80 304
WIRE 48 320 48 208
WIRE 48 320 16 320
WIRE 64 320 48 320
WIRE 128 320 128 144
WIRE 160 320 128 320
WIRE 272 320 240 320
WIRE 288 320 272 320
WIRE 416 320 368 320
WIRE 704 320 704 288
WIRE 752 320 704 320
WIRE 1120 320 1120 288
WIRE -48 336 -64 336
WIRE 864 336 864 208
WIRE 864 336 816 336
WIRE 880 336 864 336
WIRE 752 352 704 352
WIRE -64 368 -64 336
WIRE 576 384 544 384
WIRE 704 384 704 352
WIRE 704 384 656 384
WIRE 704 416 704 384
WIRE -240 448 -240 304
WIRE 160 448 -240 448
WIRE 272 448 272 320
WIRE 272 448 240 448
WIRE 304 448 272 448
WIRE 416 464 416 320
WIRE 416 464 368 464
WIRE 304 480 288 480
WIRE -464 496 -560 496
WIRE 288 512 288 480
FLAG -560 496 0
FLAG -464 416 v+
FLAG -464 576 v-
FLAG -240 208 audio
FLAG -464 320 0
FLAG -16 288 v+
FLAG 336 432 v+
FLAG -16 352 v-
FLAG 336 496 v-
FLAG 288 512 0
FLAG -64 368 0
FLAG 416 320 rect
FLAG 496 64 v+
FLAG 496 128 v-
FLAG 1120 320 0
FLAG -320 -112 v+
FLAG -320 -48 v-
FLAG -384 -32 0
FLAG 432 -96 v+
FLAG 800 -32 0
FLAG 592 96 comp
FLAG 608 -80 ref
FLAG 1248 48 v+
FLAG 1248 112 v-
FLAG 1376 80 env
FLAG 1440 192 0
FLAG 352 112 rect
FLAG 352 80 env
FLAG 880 16 diff
FLAG 784 304 v+
FLAG 784 368 v-
FLAG 880 336 diff
FLAG 544 384 rect
FLAG 544 288 env
FLAG 704 496 0
FLAG 880 96 diff
FLAG 208 -1104 v+
FLAG 208 -1040 v-
FLAG 448 -1120 v+
FLAG 448 -1056 v-
FLAG 672 -1104 v+
FLAG 672 -1040 v-
FLAG 1120 -1104 v+
FLAG 1120 -1040 v-
FLAG 208 -592 v+
FLAG 208 -528 v-
FLAG 544 -848 0
FLAG 672 -848 0
FLAG 800 -848 0
FLAG 960 -1024 0
FLAG 544 -944 v+
FLAG 672 -944 v+
FLAG 800 -944 v+
FLAG 960 -1120 v+
FLAG 16 -1248 audio
FLAG 1264 -1168 env2
FLAG 32 -864 0
FLAG 320 -752 0
FLAG 640 -400 0
FLAG 512 -912 0
FLAG 640 -912 0
FLAG 768 -912 0
FLAG 992 -848 0
FLAG 1120 -848 0
FLAG 368 -1040 0
FLAG 112 -560 0
FLAG 384 -432 0
FLAG 432 -560 v+
FLAG 1232 -544 0
FLAG 832 -592 v+
FLAG 784 -720 v+
SYMBOL voltage -464 400 R0
WINDOW 123 0 0 Left 2
WINDOW 39 0 0 Left 2
WINDOW 0 25 16 Left 2
WINDOW 3 37 50 Left 2
SYMATTR InstName V1
SYMATTR Value 12
SYMBOL voltage -464 480 R0
WINDOW 123 0 0 Left 2
WINDOW 39 0 0 Left 2
WINDOW 0 24 17 Left 2
WINDOW 3 35 63 Left 2
SYMATTR InstName V2
SYMATTR Value 12
SYMBOL voltage -464 224 R0
WINDOW 3 -231 132 Left 2
WINDOW 123 0 0 Left 2
WINDOW 39 0 0 Left 2
SYMATTR Value wavefile="./test2.wav" chan=0
SYMATTR InstName V3
SYMBOL Opamps\\LT1001 -16 256 R0
SYMATTR InstName U1
SYMBOL Opamps\\LT1001 336 400 R0
SYMATTR InstName U2
SYMBOL res -96 288 R90
WINDOW 0 0 56 VBottom 2
WINDOW 3 32 56 VTop 2
SYMATTR InstName R1
SYMATTR Value 5k
SYMBOL res 128 128 R90
WINDOW 0 0 56 VBottom 2
WINDOW 3 32 56 VTop 2
SYMATTR InstName R2
SYMATTR Value 10k
SYMBOL res 256 304 R90
WINDOW 0 0 56 VBottom 2
WINDOW 3 32 56 VTop 2
SYMATTR InstName R3
SYMATTR Value 10k
SYMBOL diode 128 304 R90
WINDOW 0 0 32 VBottom 2
WINDOW 3 32 32 VTop 2
SYMATTR InstName D1
SYMBOL res 384 304 R90
WINDOW 0 0 56 VBottom 2
WINDOW 3 32 56 VTop 2
SYMATTR InstName R4
SYMATTR Value 20k
SYMBOL res 256 432 R90
WINDOW 0 0 56 VBottom 2
WINDOW 3 32 56 VTop 2
SYMATTR InstName R5
SYMATTR Value 10k
SYMBOL diode 16 192 R90
WINDOW 0 0 32 VBottom 2
WINDOW 3 32 32 VTop 2
SYMATTR InstName D2
SYMBOL Opamps\\LT1001 496 32 R0
SYMATTR InstName U4
SYMBOL res 896 112 R270
WINDOW 0 32 56 VTop 2
WINDOW 3 0 56 VBottom 2
SYMATTR InstName R6
SYMATTR Value 330k
SYMBOL Opamps\\LT1001 -320 -144 R0
SYMATTR InstName U3
SYMBOL res -448 16 R0
SYMATTR InstName R7
SYMATTR Value 10k
SYMBOL res -256 -240 R90
WINDOW 0 0 56 VBottom 2
WINDOW 3 32 56 VTop 2
SYMATTR InstName R8
SYMATTR Value 47k
SYMBOL res 592 -96 R90
WINDOW 0 0 56 VBottom 2
WINDOW 3 32 56 VTop 2
SYMATTR InstName R9
SYMATTR Value 100k
SYMBOL res 752 -96 R90
WINDOW 0 0 56 VBottom 2
WINDOW 3 32 56 VTop 2
SYMATTR InstName R10
SYMATTR Value 10k
SYMBOL cap 1104 160 R0
SYMATTR InstName C1
SYMATTR Value 330n
SYMBOL diode 1072 80 R90
WINDOW 0 0 32 VBottom 2
WINDOW 3 32 32 VTop 2
SYMATTR InstName D3
SYMBOL diode 1008 32 R270
WINDOW 0 32 32 VTop 2
WINDOW 3 0 32 VBottom 2
SYMATTR InstName D4
SYMBOL res 896 32 R270
WINDOW 0 32 56 VTop 2
WINDOW 3 0 56 VBottom 2
SYMATTR InstName R11
SYMATTR Value 10k
SYMBOL Opamps\\LT1001 1248 16 R0
SYMATTR InstName U5
SYMBOL res 1424 96 R0
SYMATTR InstName R12
SYMATTR Value 100k
SYMBOL Opamps\\LT1001 784 272 R0
SYMATTR InstName U6
SYMBOL res 672 272 R90
WINDOW 0 0 56 VBottom 2
WINDOW 3 32 56 VTop 2
SYMATTR InstName R13
SYMATTR Value 10k
SYMBOL res 672 368 R90
WINDOW 0 0 56 VBottom 2
WINDOW 3 32 56 VTop 2
SYMATTR InstName R14
SYMATTR Value 10k
SYMBOL res 832 192 R90
WINDOW 0 0 56 VBottom 2
WINDOW 3 32 56 VTop 2
SYMATTR InstName R15
SYMATTR Value 10k
SYMBOL res 720 512 R180
WINDOW 0 36 76 Left 2
WINDOW 3 36 40 Left 2
SYMATTR InstName R16
SYMATTR Value 10k
SYMBOL res 1184 160 R0
SYMATTR InstName R17
SYMATTR Value 1000k
SYMBOL cap 48 -1104 R180
WINDOW 0 24 56 Left 2
WINDOW 3 24 8 Left 2
SYMATTR InstName C2
SYMATTR Value 470n
SYMBOL Opamps\\LT1001 208 -1136 R0
SYMATTR InstName U7
SYMBOL Opamps\\LT1001 448 -1152 R0
SYMATTR InstName U8
SYMBOL Opamps\\LT1001 672 -1136 R0
SYMATTR InstName U9
SYMBOL Opamps\\LT1001 1120 -1136 R0
SYMATTR InstName U10
SYMBOL Opamps\\LT1001 208 -624 R0
SYMATTR InstName U11
SYMBOL CD4000\\CD4066 544 -896 R0
SYMATTR InstName U12
SYMBOL CD4000\\CD4066 672 -896 R0
SYMATTR InstName U13
SYMBOL CD4000\\CD4066 800 -896 R0
SYMATTR InstName U14
SYMBOL CD4000\\CD4066 960 -1072 R0
SYMATTR InstName U15
SYMBOL CD4000\\CD4030B 496 -608 R0
SYMATTR InstName U16
SYMATTR SpiceLine VDD=12  SPEED=1.0  TRIPDT=5e-9
SYMBOL CD4000\\CD4030B 784 -544 R0
SYMATTR InstName U17
SYMATTR SpiceLine VDD=12  SPEED=1.0  TRIPDT=5e-9
SYMBOL CD4000\\CD4030B 720 -640 R180
SYMATTR InstName U18
SYMATTR SpiceLine VDD=12  SPEED=1.0  TRIPDT=5e-9
SYMBOL CD4000\\CD4030B 1120 -496 R180
SYMATTR InstName U19
SYMATTR SpiceLine VDD=12  SPEED=1.0  TRIPDT=5e-9
SYMBOL diode 288 -544 R270
WINDOW 0 32 32 VTop 2
WINDOW 3 0 32 VBottom 2
SYMATTR InstName D5
SYMBOL diode 544 -1040 R270
WINDOW 0 32 32 VTop 2
WINDOW 3 0 32 VBottom 2
SYMATTR InstName D6
SYMBOL diode 800 -1072 R270
WINDOW 0 32 32 VTop 2
WINDOW 3 0 32 VBottom 2
SYMATTR InstName D7
SYMBOL cap 304 -816 R0
SYMATTR InstName C3
SYMATTR Value 1�
SYMBOL cap 656 -400 R180
WINDOW 0 24 56 Left 2
WINDOW 3 24 8 Left 2
SYMATTR InstName C4
SYMATTR Value 100p
SYMBOL cap 1024 -656 R90
WINDOW 0 0 32 VBottom 2
WINDOW 3 32 32 VTop 2
SYMATTR InstName C5
SYMATTR Value 100p
SYMBOL res 16 -960 R0
SYMATTR InstName R18
SYMATTR Value 100k
SYMBOL res 304 -928 R0
SYMATTR InstName R19
SYMATTR Value 10k
SYMBOL res 304 -1040 R0
SYMATTR InstName R20
SYMATTR Value 1k
SYMBOL res 240 -1152 R270
WINDOW 0 32 56 VTop 2
WINDOW 3 0 56 VBottom 2
SYMATTR InstName R21
SYMATTR Value 4k7
SYMBOL res 448 -1168 R270
WINDOW 0 32 56 VTop 2
WINDOW 3 0 56 VBottom 2
SYMATTR InstName R22
SYMATTR Value 4k7
SYMBOL res 704 -1056 R270
WINDOW 0 32 56 VTop 2
WINDOW 3 -9 80 VBottom 2
SYMATTR InstName R23
SYMATTR Value 47
SYMBOL cap 976 -912 R0
SYMATTR InstName C6
SYMATTR Value 10n
SYMBOL cap 1104 -912 R0
SYMATTR InstName C7
SYMATTR Value 1n
SYMBOL res 928 -608 R90
WINDOW 0 0 56 VBottom 2
WINDOW 3 32 56 VTop 2
SYMATTR InstName R24
SYMATTR Value 22k
SYMBOL res 400 -416 R180
WINDOW 0 36 76 Left 2
WINDOW 3 36 40 Left 2
SYMATTR InstName R25
SYMATTR Value 100k
SYMBOL res 656 -560 R90
WINDOW 0 0 56 VBottom 2
WINDOW 3 32 56 VTop 2
SYMATTR InstName R26
SYMATTR Value 22k
TEXT -624 640 Left 2 !.tran 0 20 0 startup
TEXT -632 672 Left 2 !.lib CD4000.lib
TEXT -640 704 Left 2 !.inc CD4066.sub
