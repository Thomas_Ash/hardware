hardware
========

spice models, pcb layouts and firmware for a few hybrid analogue/digital synthesizer modules I designed.

(c) Thomas Ash 2007-2018 GPL