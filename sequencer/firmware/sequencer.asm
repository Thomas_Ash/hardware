;sequencer brain
;thom ash 2009


.INCLUDE "m48def.inc"
.INCLUDE "macros.inc"
.DEF temp = R16
.DEF save = R17
.DEF mode = r18 ;00 = absolute / async
				;01 = absolute / synced
				;10 = relative / async
				;11 = relative / synced
.DEF stage = r19
.DEF nextstage = r20

.CSEG

;interrupt vectors
.ORG 0x0000
rjmp reset
.ORG 0x0001
rjmp int_clock
.ORG 0x0002
rjmp int_stage
.ORG 0x000A
rjmp reset
.ORG 0x000B
rjmp end_pulse
.ORG 0x0015
rjmp int_sample

;reset 
.ORG 0x001a
reset:
;set up stack
ldi temp, high(RAMEND)
STORE SPH,temp
ldi temp, low(RAMEND)
STORE SPL,temp

;set up timer

ldi temp, 0b00000000
STORE TCCR1A, temp
STORE TCCR1B, temp		;stopped, 0b00000101 to go
STORE TCCR1C, temp
STORE TCNT1L, temp
STORE TCNT1H, temp
ldi temp, 0x00			
STORE OCR1AL, temp
ldi temp, 0x01			
STORE OCR1AH, temp
ldi temp, 0b00000010
STORE TIMSK1, temp


;init I/O
;port B0-4: digital out
LDI temp, 0b00011111
STORE DDRB, temp

LDI temp, 0b00000000
STORE PORTB, temp

;port C0: analog in
LDI temp, 0b00000000
STORE DDRC, temp
STORE PORTC, temp

;...and set up ADC
LDI temp, 0b00100000
STORE ADMUX, temp

LDI temp, 0b10000111
STORE ADCSRA, temp

LDI temp, 0b00000000
STORE ADCSRB, temp

LDI temp, 0b00000001
STORE DIDR0, temp

;port D0-7: digital in
LDI temp, 0b00000000
STORE DDRD, temp
STORE PORTD, temp

;...and set up interrupts
LDI temp, 0b00001111 
STORE EICRA, temp

LDI temp, 0b00000011
STORE EIMSK, temp

;enable interrupts
sei


CLR temp
CLR save
CLR mode
CLR stage
CLR nextstage


main:
	;check mode
	LOAD mode, PIND
	ANDI mode, 0b00000011


	;start conversion
	LDI temp, 0b11000111
	STORE ADCSRA, temp


	;wait for result
	wait:

		LOAD temp, ADCSRA
		CPI temp, 0b10010111
	BRNE wait

	;tidy up data
	LOAD temp, ADCH
	SWAP temp
	ANDI temp, 0b00001111


	SBRC mode, 1
		ADD temp, stage 	;either relative mode

	SBIS PIND, 3
		MOV nextstage, temp		;allow stage select board to override	

	CPI mode, 0b00000000
	BRNE main

	RCALL change_stage	;absolute&async only

RJMP main

int_clock:
	LOAD save, SREG
	PUSH temp
	
	RCALL change_stage

		
	POP temp
	STORE SREG, save	
RETI

int_stage:
	LOAD save, SREG
	PUSH temp
	
	LOAD temp, PIND
	SWAP temp
	ANDI temp, 0b00001111

	MOV nextstage, temp		

	SBRS mode ,0
		RCALL change_stage
		
	POP temp
	STORE SREG, save
RETI

int_sample:
RETI

end_pulse:
	LOAD save, SREG
	PUSH temp
	
	CBI PORTB, 4	;end pulse

	LDI temp, 0b00000000	;halt timer
	STORE TCCR1B, temp
		
	POP temp
	STORE SREG, save	
RETI

change_stage:
	LOAD save, SREG
	PUSH temp
	
	CP stage, nextstage		;are we trying to 'change' to the same stage?
	BREQ nochange			;if so nothing needs to be done...

		MOV stage, nextstage	;update current stage register

		MOV temp, nextstage		;prepare for output
		ORI temp, 0b00010000	;set the clock state high

		STORE PORTB, temp			;put on PORTB

		LDI temp, 0b00000000   ;reset timer
		STORE TCNT1L, temp		
		STORE TCNT1H, temp
		LDI temp, 0b00000010	;start timer
		STORE TCCR1B, temp


	nochange:
	POP temp
	STORE SREG, save	
RET

